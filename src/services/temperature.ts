import http from '@/services/http'

type ReturnData = {
  celsius: number
  fahrenheit: number
}

async function convert(celsius: number): Promise<number> {
  console.log('Service: call Convert')
  console.log(`/temperature/convert/${celsius}`)
  const res = await http.post(`/temperature/convert/`, {
    celsius: celsius
  })
  const convertResult = res.data as ReturnData
  console.log('Service: Finish call Convert')
  return convertResult.fahrenheit
}

export default { convert }
